package com.salesforce;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.io.BufferedInputStream;
import java.util.*;

/**
 * System dependencies tool, a Salesforce.com coding test for Jeremy Dashe (jeremy@dashe.net)
 * <p/>
 * We want a program to automate the process of adding and removing components. To do this we
 * will maintain a record of installed components and component dependencies. A component can be
 * installed explicitly in response to a command (unless it is already installed), or implicitly
 * if it is needed for some other component being installed. Likewise, a component, not explicitly
 * installed, can be explicitly removed in response to a command (if it is not needed to support
 * other components) or implicitly removed if it is no longer needed to support another component.
 *
 * Input
 * The input will contain a sequence of commands (as described below), each on a separate line
 * containing no more than eighty characters. Item names are case sensitive, and each is no longer
 * than ten characters. The command names (DEPEND, INSTALL, REMOVE and LIST) always appear in uppercase
 * starting in column one, and item names are separated from the command name and each other by one or
 * more spaces. All appropriate DEPEND commands will appear before the occurrence of any INSTALL dependencies.
 * The end of the input is marked by a line containing only the word END.
 *
 * We'll read from STDIN for the input.
 *
 * Command Syntax 	Interpretation/Response
 *   DEPEND item1 item2 [item3 ...] 	item1 depends on item2 (and item3 ...)
 *   INSTALL item1 	install item1 and those on which it depends
 *   REMOVE item1 	remove item1, and those on which it depends, if possible
 *   LIST 	list the names of all currently-installed components
 *
 * Output
 * Echo each line of input. Follow each echoed INSTALL or REMOVE line with the actions taken in response,
 * making certain that the actions are given in the proper order. Also identify exceptional conditions (see
 * Expected Output, below, for examples of all cases). For the LIST command, display the names of the
 * currently installed components. No output, except the echo, is produced for a DEPEND command or the line
 * containing END. There will be at most one dependency list per item.
 *
 */
public class SystemDependencies {
  private static final java.lang.String DELIMITER = " ";

  // Valid commands
  enum Command {DEPEND, INSTALL, REMOVE, LIST, END}

  //
  // Representation of a single item.  Items have a unique name (which we'll use as the key),
  // and a state of being installed or not.  Relations between items will be kept as collections of items
  // depended upon (downstream dependencies), and items that depend on this node (upstream dependencies).
  //
  public static class Item {
    private final String name;
    private Collection<Item> upstreamDependents = new HashSet<>();
    private Collection<Item> downstreamDependents = new HashSet<>();
    private boolean isInstalled = false;

    /**
     * Creates a new, uninstalled Item.
     * @param name Unique, non-null name.
     */
    public Item(String name) {
      Preconditions.checkArgument(!Strings.isNullOrEmpty(name));
      this.name = name;
    }

    public String toString() {
      return name;
    }
  }

  public static void main (String[] args) {
    // Main item set, mapped by item name.
    Map<String, Item> items = new HashMap<>();


    // Read from STDIN, building a directed acyclic graph of the dependencies as we go.  Separate lines
    // indicate that we should update the DAG and send any output required; END or <EOF> indicates we
    // are done and can exit.

    // Input errors ideally will give a diagnostic message then keep on going.

    // There are two things we need to track: the dependencies, and the state of what's installed and what's not.
    // We'll use a boolean to indicate installed state; could use an enum if we need more elaborate state.

    // First off, set up dependencies with the DEPEND command.  For each DEPEND:
    //  - See if the depended-upon item already exists.  If not, create it.
    //    - If not yet exists, then easy: create the item, add to the main map, and add dependents, creating as needed.
    //    - If exists, add dependents, checking for cycles. Reject if there's a cycle. (Though this isn't mentioned in the problem statement; put off for later)

    // Get command and items.  Much nicer ways to do this...
    Scanner stdin = new Scanner(new BufferedInputStream(System.in));

    while (stdin.hasNext()) {
      InputLine nextLine = getNextLine(stdin);
      if (nextLine == null) {
        continue;
      }

      switch (nextLine.command) {
        case DEPEND:
          List<String> lineItems = nextLine.items;
          if (lineItems.size() < 2) {
            // illegal, no items... barf in the expected manner.
            continue;
          }
          addDependency(items, lineItems.get(0), lineItems.subList(1, lineItems.size()));
          System.out.printf("%s\n", nextLine.rawInput);
          break;

        case INSTALL:
          // Run through upstream dependencies recursively first if they're not yet installed (simple traversal with visited set),
          // then set flag.
          install(items, nextLine.items.get(0));

          break;
        case REMOVE:
          break;
        case LIST:
          listInstalled(items);
          break;
        case END:
          System.out.println("\nExiting.");
          return; // all done!
      }
    }

  }

  // Sets the install flag on the named item.  If the item exists and depends upon other items, recursively install them first,
  // checking against already installing.
  private static void install (Map<String, Item> items, String item) {

  }

  private static void listInstalled (Map<String, Item> items) {
    for (String itemName : items.keySet()) {
      Item item = items.get(itemName);
      System.out.printf("%s, %s installed, dependents %s, depends upon %s\n", itemName, item.isInstalled ? "IS" : "is NOT", item.downstreamDependents, item.upstreamDependents);
    }
  }

  private static void addDependency (Map<String, Item> items, String dependedUpon, List<String> dependents) {
    if (!items.containsKey(dependedUpon)) {
      Item newItem = new Item(dependedUpon);
      Collection<Item> dependentItems = getItems(items, dependents);
      newItem.downstreamDependents.addAll(dependentItems);
      for (Item item : dependentItems) {
        item.upstreamDependents.add(newItem);
      }
      items.put(dependedUpon, newItem);
      // all done.
      return;
    }

    // This item already exists.  Add the named dependents.  We'd check for cycles here, btw.
    // TODO: Check for cycles.
    Item dependency = items.get(dependedUpon);
    Collection<Item> dependentItems = getItems(items, dependents);
    dependency.downstreamDependents.addAll(dependentItems);
    for (Item item : dependentItems) {
      item.upstreamDependents.add(dependency);
    }

  }


  // Returns a Collection of named Items, adding them into the items list if they don't yet exist,
  // referencing existing ones if they're present.
  private static Collection<Item> getItems (Map<String, Item> items, List<String> dependents) {
    Preconditions.checkNotNull(items);
    Preconditions.checkNotNull(dependents);

    Collection<Item> results = new ArrayList<>(dependents.size());
    for (String itemName : dependents) {

      Item newItem = (items.containsKey(itemName)) ? items.get(itemName) : new Item(itemName);
      results.add(newItem);
    }

    return results;
  }


  /////////////////////////////////////////
  // I/O stuff
  /////////////////////////////////////////

  private static class InputLine {
    public final String rawInput;
    public final Command command;
    public final List<String> items = new ArrayList<>(); // could be made more space-efficient with a lazy load

    // Note: throws if command doesn't match exactly.
    public InputLine(String rawInput, String command) {
      Preconditions.checkArgument(!Strings.isNullOrEmpty(rawInput));
      Preconditions.checkArgument(!Strings.isNullOrEmpty(command));
      this.rawInput = rawInput;
      this.command = Command.valueOf(command);
    }
  }

  // Returns the next command and items from input.
  private static InputLine getNextLine (Scanner input) {
    String line = input.nextLine();
    if (Strings.isNullOrEmpty(line)) {
      return null;
    }

    // Break string up by delimiters -- spaces.
    String[] commandAndItems = line.split(DELIMITER);
    if (commandAndItems.length < 1) {
      return null;
    }

    InputLine result = new InputLine(line, commandAndItems[0]);
    for (int i = 1; i < commandAndItems.length; i++) {
      if (commandAndItems[i].length() < 1) {
        // skip spaces.
        continue;
      }
      result.items.add(commandAndItems[i]);
    }

    return result;

  }

  public static String echo (String toBeEchoed) {
    return toBeEchoed;
  }
}
