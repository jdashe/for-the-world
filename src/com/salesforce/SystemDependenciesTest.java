package com.salesforce;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotSame;

public class SystemDependenciesTest {

  @BeforeMethod
  public void setUp () throws Exception {

  }

  @AfterMethod
  public void tearDown () throws Exception {

  }

  @Test
  public void testMain() {
    System.out.println("Hello, test world!");
    assertEquals(true, true);
  }

  @Test
  public void testEcho() {
    String testValue = "Oh, hi!";
    assertEquals(testValue, SystemDependencies.echo(testValue));

    assertNotSame(testValue, SystemDependencies.echo(null));
  }
}